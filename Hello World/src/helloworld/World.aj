package helloworld;

public aspect World {
pointcut greeting() : execution(* Hello.sayHello(..));
private pointcut mainMethod() :
	execution(public static void main(String[]));
before () : mainMethod() {
	System.out.println("> " + thisJoinPoint);
}
after() : mainMethod(){
	System.out.println("< "+ thisJoinPoint);
}
after() returning() : greeting() {
	System.out.println(" World!");
}
}
