package ro.trace;

public class Square extends TwoDShape {

	protected double s;

	public Square(double x, double y, double s) {
		super(x, y);
		this.s = s;
	}

	public Square(double x, double y) {
		this(x, y, 1.0);
	}

	public Square( double s) {
		this(0.0, 0.0, s);
	}

	public Square() {
		this(0.0, 0.0, 1.0);
	}

	public double perimeter() {
		return 4 * s;
	}

	public double area() {
		return s * s;
	}

	public String toString() {
		return ("Square side = " + String.valueOf(s) + super.toString());
	}
}
