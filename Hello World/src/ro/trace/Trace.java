package ro.trace;

import java.io.PrintStream;

public class Trace {
	public static int TRACELEVEL = 0;
	protected static PrintStream stream = null;
	protected static int callDepth = 0;
	public static void initStream(PrintStream s){
		stream = s;
	}
	public static void traceEntry(String str) {
		if(TRACELEVEL == 0) return;
		if(TRACELEVEL == 2)callDepth++;
		printEntering(str);
	}
	public static void traceExit(String str) {
		if(TRACELEVEL == 0)return;
		printExiting(str);
		if(TRACELEVEL ==2) callDepth--;
	}
	
	private static void printEntering(String str){
		printIndent();
		stream.println("--> "+ str);
	}
	
	private static void printExiting(String str) {
		printIndent();
		stream.println("<-- " + str);
	}
	
	private static void printIndent(){
		for(int i =0; i < callDepth; i++)
			stream.print(" ");
	}
}
