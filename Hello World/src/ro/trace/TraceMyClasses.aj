package ro.trace;

public aspect TraceMyClasses {
	pointcut myClass() : within(TwoDShape)||within(Circle)||within(Square);
	pointcut myConstructor() : myClass() && execution(new(..));
	pointcut myMethod() : myClass() && execution(* *(..));
	
	before() : myConstructor() {
		Trace.traceEntry("cons " + thisJoinPointStaticPart.getSignature());
	}
	after() : myConstructor(){
		Trace.traceExit("cons " + thisJoinPointStaticPart.getSignature());
	}
	before():myMethod(){
		Trace.traceEntry("me " + thisJoinPointStaticPart.getSignature());
	}
	after():myMethod(){
		Trace.traceExit("me " + thisJoinPointStaticPart.getSignature());
	}
	
	public static void main(String[] args){
		Trace.TRACELEVEL = 2;
		Trace.initStream(System.err);
		ExampleMain.main(args);
	}
}
