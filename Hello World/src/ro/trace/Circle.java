package ro.trace;

public class Circle extends TwoDShape{
 protected double r;
 
 public Circle(double x, double y, double r){
	 super(x,y); this.r= r;
 }
 
 public Circle(double x, double y){
	 this(x,y,1.0);
 }
 
 public Circle(double r){
	 this(0.0, 0.0,r);
 }
 
 public Circle(){
	 this(0.0, 0.0, 1.0);
 }
 
 public double perimeter(){
	 return 2 * Math.PI*r;
 }
 
 public double area(){
	 return Math.PI * r *r;
 }
 
 public String toString() {
	 return ("Circle radius = " + String.valueOf(r)
			 + super.toString());
 }
}
