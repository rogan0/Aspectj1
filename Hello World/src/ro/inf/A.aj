package ro.inf;

public aspect A {
before() : call( * Main.* (..))  {System.out.println("before");}
after() returning: call(* Main.* (..))  {System.out.println("after");}
}
