package ro.ob;

public aspect SubjectObserverProtocoImpl extends SubjectObserverProtocol{

	declare parents: Button implements Subject;
	public Object Button.getData() { return this;}
	
	declare parents: ColorLabel implements Observer;
	public void ColorLabel.update(){
		colorCycle();
	}
	
	pointcut stateChanges(Subject s):
		target(s) &&
		call(void Button.click());
}
