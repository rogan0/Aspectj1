package ro.ob;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button extends java.awt.Button{
static final Color defaultBackgroundColor = Color.gray;
static final Color defaultForegroundColor = Color.black;
static final String defaultText = "cycle color";

Button(Display display){
	super();
	setLabel(defaultText);
	setBackground(defaultBackgroundColor);
	setForeground(defaultForegroundColor);
	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			Button.this.click();
		}
	});
	display.addToFrame(this);
}

public void click() {}
}
