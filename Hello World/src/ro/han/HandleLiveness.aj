package ro.han;

aspect HandleLiveness{
	before(Handle handle):target(handle) && call(public * * (..)){
		if(handle.partner == null || !handle.partner.isAlive()){
			throw new DeadPartnerException();
		}
	}
}
