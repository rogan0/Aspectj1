package ro.han;

public class Handle {
Partner partner = new Partner();
public void foo() { partner.foo(); }
public void bar(int x) {
	partner.bar(x);
}

public static void main(String[] args){
	Handle h1 = new Handle();
	h1.foo();
	h1.bar(2);
}
}

class Partner {
	boolean isAlive() {
		return true;
	}
	void foo(){ System.out.println("foo");}
	void bar(int x) { System.out.println("bar "+x);}
}


class DeadPartnerException extends RuntimeException{
	
}