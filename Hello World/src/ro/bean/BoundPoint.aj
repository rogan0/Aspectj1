package ro.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public aspect BoundPoint {
    private PropertyChangeSupport Point.support = new PropertyChangeSupport(this);
    
    public void Point.addPropertyChangeListener(PropertyChangeListener listener){
    	support.addPropertyChangeListener(listener);
    }
    
    public void Point.addPropertyChangeListener(String propertyName,
    		PropertyChangeListener listener){
    	support.addPropertyChangeListener(propertyName, listener);
    };
    
    public void Point.removePropertyChangeListener(String propertyName, PropertyChangeListener listener){
    	support.removePropertyChangeListener(propertyName, listener);
    }
    
    public void Point.removePropertyChangeListener(PropertyChangeListener listener){
    	support.removePropertyChangeListener(listener);
    }
    
    public void Point.hasListeners(String propertyName){
    	support.hasListeners(propertyName);
    }
    
    declare parents: Point implements Serializable;
    
    void around(Point p): execution(void Point.setX(int)) && target(p){
    	int oldValue = p.getX();
    	proceed(p);
    	firePropertyChange(p,"x",oldValue, p.getX());
    }
    
    void around(Point p): execution(void Point.setY(int)) && target(p){
    	int oldVaule = p.getY();
    	proceed(p);
    	firePropertyChange(p,"y",oldVaule, p.getY());
    }
    
    void firePropertyChange(Point p,
    		String property,
    		double oldval,
    		double newval){
    	p.support.firePropertyChange(property,
    			new Double(oldval),
    			new Double(newval));
    }
}
