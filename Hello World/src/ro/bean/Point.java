package ro.bean;

public class Point {
    protected int x = 0;
    protected int y = 0;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public void setRectangular(int newX, int newY){
		setX(newX);
		setY(newY);
	}
	
	public void offset(int deltaX, int deltaY){
		setRectangular(x + deltaX, y + deltaY);
	}
	
	public String toString(){
		return "(" + getX() + ", " + getY() + ")";
	}
}
