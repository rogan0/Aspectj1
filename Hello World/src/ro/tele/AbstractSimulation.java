package ro.tele;

public abstract class AbstractSimulation {
	public static AbstractSimulation simulation;
	
	public void run() {
		Customer jim =new Customer("Jim",650);
		Customer mik = new Customer("Mik", 650);
		Customer crista = new Customer("Crista", 415);
		
		say("jim calls mik...");
		Call c1 = jim.call(mik);
		wait(1.0);
		say("mik accepts...");
		mik.pickup(c1);
		wait(2.0);
		say("jim hangs up...");
		jim.hangup(c1);
		report(jim);
		report(mik);
		report(crista);
		
		say("mik calls crista...");
		Call c2 = mik.call(crista);
		say("crista accepts...");
		crista.pickup(c2);
		wait(1.5);
		say("crista hangs up...");
		crista.hangup(c2);
		report(jim);
		report(mik);
		report(crista);
	}
	
	abstract protected void report(Customer c);
	
	protected static void wait(double seconds){
		Object dummy = new Object();
		synchronized (dummy){
			try{dummy.wait((long)(seconds*100));}
			catch(Exception e){}
		}
	}
	
	protected static void say(String s){
		System.out.println(s);
	}
}
