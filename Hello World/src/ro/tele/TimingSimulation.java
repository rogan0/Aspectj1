package ro.tele;

public class TimingSimulation extends AbstractSimulation {
     
	public static void main(String[] args) {
		System.out.println("\n... Timing simulation 2 ...\n");
		simulation = new TimingSimulation();
		simulation.run();
	}
	
	protected void report(Customer c){
		Timing t = Timing.aspectOf();
		System.out.println(c + " spent " + t.getTotalConnectTime(c));
	}
}
