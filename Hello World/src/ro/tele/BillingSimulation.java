package ro.tele;

public class BillingSimulation extends AbstractSimulation{
	
	public static void main(String[] args){
		System.out.println("\n...Billing simulation 2 ...\n");
		simulation = new BillingSimulation();
		simulation.run();
	}
	
	protected void report(Customer c){
		Timing t = Timing.aspectOf();
		Billing b = Billing.aspectOf();
		System.out.println(c+" has been connected for "
				+  t.getTotalConnectTime(c)
				+ " seconds and has a bill of "
				+ b.getTotalCharge(c));
	}
}
