package ro.tele;

import java.util.Enumeration;
import java.util.Vector;

public class Call {
  private Customer caller, receiver;
  private Vector connections = new Vector();
  
  public Call(Customer caller, Customer receiver){
	  this.caller = caller;
	  this.receiver = receiver;
	  Connection c;
	  if(receiver.localTo(caller)){
		  c= new Local(caller, receiver);
	  } else {
		  c = new LongDistance(caller, receiver);
	  }
	  connections.addElement(c);
  }
  
  public void pickup() {
	  Connection connection = (Connection) connections.lastElement();
	  connection.complete();
  }
  
  public boolean isConnected(){
	  return ((Connection) connections.lastElement()).getState()
			  == Connection.COMPLETE;
  }
  
  public void hangup(Customer c){
	  for(Enumeration  e= connections.elements(); e.hasMoreElements();){
		  ((Connection) e.nextElement()).drop();
	  }
  }
  
  public boolean includes(Customer c){
	  boolean result = false;
	  for(Enumeration e = connections.elements();e.hasMoreElements();){
		  result = result || ((Connection) e.nextElement()).connects(c);
	  }
	  return result;
  }
  
  public void merge(Call other){
	  for(Enumeration e = other.connections.elements();e.hasMoreElements();){
		  Connection conn = (Connection) e.nextElement();
		  other.connections.removeElement(conn);
		  connections.addElement(conn);
	  }
  }
}
