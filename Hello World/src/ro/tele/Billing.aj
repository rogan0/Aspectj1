package ro.tele;

public aspect Billing {

	declare precedence:Billing, Timing;
	public static final long LOCAL_RATE = 3;
	public static final long LONG_DISTANCE_RATE = 10;

	public Customer Connection.payer;

	public Customer getPayer(Connection conn) {
		return conn.payer;
	}

	after(Customer cust) returning (Connection conn):
			args(cust, ..) && call(Connection+.new(..)){
		conn.payer = cust;
	}

	public abstract long Connection.callRate();
	
	public long LongDistance.callRate() { return LONG_DISTANCE_RATE; }
	public long Local.callRate() { return LOCAL_RATE; }
	
	after(Connection conn) : Timing.endTiming(conn) {
		long time = Timing.aspectOf().getTimer(conn).getTime();
		long rate = conn.callRate();
		long cost = rate * time;
		getPayer(conn).addCharge(cost);
	}
	
	public long Customer.totalCharge = 0;
	public long getTotalCharge(Customer cust) { return cust.totalCharge; }
	
	public void Customer.addCharge(long charge){
		totalCharge += charge;
	}
}
