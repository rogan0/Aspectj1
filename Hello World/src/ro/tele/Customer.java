package ro.tele;

import java.util.Vector;

public class Customer {
	private String name;
	private int areacode;
	private Vector calls = new Vector();

	protected void removeCall(Call c){
		calls.removeElement(c);
	}
	
	protected void addCall(Call c){
		calls.addElement(c);
	}
	
	public Customer(String name, int areaacode){
		this.name = name;
		this.areacode = areacode;
	}
	
	public String toString(){
		return name+"("+areacode+")";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAreacode() {
		return areacode;
	}

	public void setAreacode(int areacode) {
		this.areacode = areacode;
	}

	public boolean localTo(Customer other){
		return areacode == other.areacode;
	}
	
	public Call call(Customer receiver){
		Call call = new Call(this, receiver);
		addCall(call);
		return call;
	}
	
	public void pickup(Call call){
		call.pickup();
		addCall(call);
	}
	
	public void hangup(Call call){
		call.hangup(this);
		removeCall(call);
	}
	
	public void merge(Call call1, Call call2){
		call1.merge(call2);
		removeCall(call2);
	}
	
	public Vector getCalls() {
		return calls;
	}

	public void setCalls(Vector calls) {
		this.calls = calls;
	}
}
