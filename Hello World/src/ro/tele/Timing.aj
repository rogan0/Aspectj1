package ro.tele;

public aspect Timing {
	
	public long Customer.totalConnectTime = 0;
	
	public long getTotalConnectTime(Customer cust){
		return cust.totalConnectTime;
	}
	
	private Timer Connection.timer = new Timer();
	public Timer getTimer(Connection conn) { return conn.timer; }
	
	after(Connection c): target(c) && call(void Connection.complete()){
		getTimer(c).start();
	}
	
	pointcut endTiming(Connection c):target(c) &&
	call(void Connection.drop());
	
	after(Connection c):endTiming(c){
		getTimer(c).stop();
		c.getCaller().totalConnectTime += getTimer(c).getTime();
		c.getReceiver().totalConnectTime += getTimer(c).getTime();
	}
}
