package ro.tele;

public abstract class Connection {
    
	public static final int PENDING = 0;
	public static final int COMPLETE = 1;
	public static final int DROPPED = 2;
	
	Customer caller, receiver;
	private int state = PENDING;
	
	Connection(Customer a, Customer b){
		this.caller = a;
		this.receiver = b;
	}
	
	public int getState(){
		return state;
	}
	
	public Customer getCaller(){ return caller; }
	
	public Customer getReceiver() { return receiver; }
	
	void complete(){
		state = COMPLETE;
		System.out.println("connection completed");
	}
	
	void drop() {
		state = DROPPED;
		System.out.println("connection dropped");
	}
	
	public boolean connects(Customer c){
		return (caller == c|| receiver == c);
	}
}
