package ro.tele;

public aspect TimerLog {
	
	after(Timer t):target(t) && call(* Timer.start()){
		System.err.println("Timer started: " + t.startTime);
	}
	
	after(Timer t) : target(t) && call(* Timer.stop()){
		System.err.println("Timer stopped: "+ t.stopTime);
	}
}
